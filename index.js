const fs = require("fs");
const path = require("path");
const argv = require("./argv");

if (!argv.validate(["dir", "ext", "format"])) {
  throw new Error("Incorrect arguments!");
}
