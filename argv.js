const args = process.argv.slice(2);

function argv(arg) {
  let index = args.indexOf("--" + arg);
  if (index !== -1 && args[index + 1] !== undefined) {
    return args[index + 1];
  } else {
    return null;
  }
}

function validateArgs(args) {
  let valid = true;
  args.map((item) => {
    if (!argv(item)) {
      valid = false;
    }
  });
  return valid;
}

module.exports = { get: argv, validate: validateArgs };
